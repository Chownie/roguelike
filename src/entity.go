package main

type Entity struct {
	Position *map[string]int
	Stats    *map[string]int
	Tags     *[]string
}

func NewEntity(Position map[string]int, Stats map[string]int, Tags []string) Entity {
	ent := Entity{&Position, &Stats, &Tags}
	return ent
}
