package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_image"
	"path/filepath"
	"encoding/json"
	"io/ioutil"
)

type Sprite struct {
	Name   string
	X      int
	Y      int
	Width  int
	Height int
}

func (sp *Sprite) GetRect() *sdl.Rect {
	return &sdl.Rect{int32(sp.X), int32(sp.Y), int32(sp.Width), int32(sp.Height)}
}

func (sp *Sprite) GetSize() *sdl.Rect {
	return &sdl.Rect{0, 0, int32(sp.Width), int32(sp.Height)}
}

type ImageAsset struct {
	DataStore   []Sprite
	Data        *sdl.Surface
	Path        string
}

func (as *ImageAsset) GetSprite(name string) Sprite {
	for i := 0; i < len(as.DataStore); i++ {
		if(as.DataStore[i].Name == name) {
			return as.DataStore[i]
		}
	}
	return Sprite{}
}

func (as *ImageAsset) GetVisual(name string) *sdl.Surface {
	for i := 0; i < len(as.DataStore); i++ {
		if(as.DataStore[i].Name == name) {
			imager, err := sdl.CreateRGBSurface(0, int32(as.DataStore[i].Width),
											  int32(as.DataStore[i].Height),
											  32, 0, 0, 0, 0)
			if err != nil {
				panic(err)
			}
			dataRect := as.DataStore[i].GetRect()
			destRect := as.DataStore[i].GetSize()
			as.Data.Blit(dataRect, imager, destRect)
			return imager
		}
	}
	return nil
}

func (as *ImageAsset) Reload() {
	temp := loadImageAsset(as.Path)
	as = &temp
}

func NewImageAsset(path string) ImageAsset {
	assetPath := filepath.Join(sdl.GetBasePath(), path)
	return loadImageAsset(assetPath)
}

func loadImageAsset(path string) ImageAsset {
	data, err := img.Load(filepath.Join(path, "data.png"))
	
	if err != nil {
		panic(err)
	}

	infoBlob, err := ioutil.ReadFile(filepath.Join(path, "info.json"))

	if err != nil {
		panic(err)
	}
	
	asset := ImageAsset{Data: data, Path: path}
	json.Unmarshal(infoBlob, &asset.DataStore)

	return asset
}