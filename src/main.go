package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_image"
)

func main() {
	sdl.Init(sdl.INIT_EVERYTHING)
	img.Init(img.INIT_PNG)
	
	window, err := sdl.CreateWindow("test",
		sdl.WINDOWPOS_UNDEFINED,
		sdl.WINDOWPOS_UNDEFINED,
		800, 600, sdl.WINDOW_SHOWN)

	if err != nil {
		panic(err)
	}
	defer window.Destroy()

	surface, err := window.GetSurface()
	if err != nil {
		panic(err)
	}
	
	ass := NewImageAsset("sprite")
	
	pictext := "coyote"
	
	picture := ass.GetVisual(pictext)
	
	running := true
	var ev sdl.Event
	for running {	
		for ev = sdl.PollEvent(); ev != nil; ev = sdl.PollEvent() {
			switch t := ev.(type) {
			case *sdl.QuitEvent:
				running = false
			case *sdl.KeyUpEvent:
				if(t.Keysym.Sym == sdl.K_ESCAPE) {
					running = false
				}
				if(t.Keysym.Sym == sdl.K_RIGHT) {
					pictext = "weed"
					picture = ass.GetVisual(pictext)
				}
			}

			destRect := sdl.Rect{150, 150, 16, 16}
			spt := ass.GetSprite(pictext)
			picture.Blit(spt.GetSize(), surface, &destRect)
			window.UpdateSurface()

			sdl.Delay(1/60)
		}
	}
	
	img.Quit()
	sdl.Quit()
}
